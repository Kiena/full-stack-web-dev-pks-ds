<?php
//Trait hewan, fight
trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function __construct($nama, $keahlian, $jumlahKaki) {
      $this->nama = $nama;
      $this->keahlian = $keahlian;
      $this->jumlahKaki = $jumlahKaki;
    }

    public function atraksi() {
      echo $this->nama . " sedang " . $this->keahlian;
    }
}
  
trait Fight {
    public $attackPower = 50;
    public $defencePower = 100;

    public function serang() {
        echo $this->nama . ", sedang menyerang ";
    }

    public function diserang() {
    
    }
}

class elang {
use Hewan, Fight;
    
    public function getInfoHewan(){
        echo "Jenis : " . $this->nama . ", Jumlah Kaki : " . $this->jumlahKaki . ", Keahlian : " . $this->keahlian . ", Attack Power : " . $this->attackPower . ", Defence Power " . $this->defencePower;
    }
}

class harimau {
    use Hewan, Fight;

        public function getInfoHewan(){
            echo "Jenis : " . $this->nama . ", Jumlah Kaki : " . $this->jumlahKaki . ", Keahlian : " . $this->keahlian . ", Attack Power : " . $this->attackPower . ", Defence Power " . $this->defencePower;
        }
    }

// Elang
$elang = new elang("Elang", "terbang tinggi", 2);
echo "<br>";
$elang->atraksi();
echo "<br>";
$elang->getInfoHewan();
echo "<br>";
$elang->serang();
$elang->diserang();

echo "<br>";

// Harimau
$harimau = new harimau("harimau", "lari cepat", 4);
echo "<br>";
$harimau->atraksi();
echo "<br>";
$harimau->getInfoHewan();
echo "<br>";
$harimau->serang();
$harimau->diserang();

?>