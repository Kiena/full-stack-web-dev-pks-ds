<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

/**
 * route resource post
 */
Route::apiResource('/post', 'PostController');

/**
 * route resource comment
 */
Route::apiResource('/comment', 'CommentController');

/**
 * route resource role
 */
Route::apiResource('/role', 'RoleController');
